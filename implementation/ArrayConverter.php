<?php

declare(strict_types=1);

namespace implementation;

require(__DIR__ . '/../domain/IArrayUtil.php');

use domain\IArrayUtil;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;

class ArrayConverter implements IArrayUtil
{
    public static function dataDecode(array $data): array
    {
        $result = [];

        foreach ($data as $key => $value) {
            $entries = explode('.', $key);

            for ($i = count($entries) - 1; $i >= 0; $i--) {
                $value = [$entries[$i] => $value];
            }

            $result[] = $value;
        }

        return array_merge_recursive(...$result);
    }

    public static function dataEncode(array $data): array
    {
        $result = [];

        $arrayIterator = new RecursiveIteratorIterator(
            new RecursiveArrayIterator($data)
        );

        foreach ($arrayIterator as $finalValue) {
            $keys   = [];
            $layers = $arrayIterator->getDepth();

            for ($depth = 0; $depth <= $layers; $depth++) {
                $layer = $arrayIterator->getSubIterator($depth);

                if ($layer !== null) {
                    $keys[] = $layer->key();
                }
            }

            $dotKey = implode('.', $keys);

            $result[$dotKey] = $finalValue;
        }

        return $result;
    }
}