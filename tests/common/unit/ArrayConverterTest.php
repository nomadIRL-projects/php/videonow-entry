<?php

declare(strict_types=1);

require(__DIR__ . '/../../../implementation/ArrayConverter.php');

use implementation\ArrayConverter;

/**
 * Никакой фреймворк не используется, просто скрипт, который проверяет корректность поведения.
 * Дата-провайдеры здесь также берут на себя роль ожидаемого вывода друг для друга,
 * но я думаю это не страшно для такого самописного теста.
 */

const DATA_DECODE_DATA_PROVIDER = [
    [
        'parent.child.field'      => 1,
        'parent.child.field2'     => 2,
        'parent2.child.name'      => 'test',
        'parent2.child2.name'     => 'test',
        'parent2.child2.position' => 10,
        'parent3.child3.position' => 10,
    ],
];

const DATA_ENCODE_DATA_PROVIDER = [
    [
        'parent'  => [
            'child' => [
                'field'  => 1,
                'field2' => 2,
            ],
        ],
        'parent2' => [
            'child'  => [
                'name' => 'test',
            ],
            'child2' => [
                'name'     => 'test',
                'position' => 10,
            ],
        ],
        'parent3' => [
            'child3' => [
                'position' => 10,
            ],
        ],
    ],
];

function testDataDecodeAndItsOk(): void
{
    foreach (DATA_DECODE_DATA_PROVIDER as $case => $dataset) {
        $actual = json_encode(
            ArrayConverter::dataDecode($dataset)
        );

        $expected = json_encode(DATA_ENCODE_DATA_PROVIDER[$case]);

        assert(
            $actual === $expected,
            <<<DESC
            Case $case failed for `dataDecode` assertion;
            
            EXPECTED:
            --
            $expected
            --
            
            GOT:
            --
            $actual
            --
            DESC
        );
    }
}

function testDataEncodeAndItsOk(): void
{
    foreach (DATA_ENCODE_DATA_PROVIDER as $case => $dataset) {
        $actual = json_encode(
            ArrayConverter::dataEncode($dataset)
        );

        $expected = json_encode(DATA_DECODE_DATA_PROVIDER[$case]);

        assert(
            $actual === $expected,
            <<<DESC
            Case $case failed for `dataEncode` assertion;
            
            EXPECTED:
            --
            $expected
            --
            
            GOT:
            --
            $actual
            --
            DESC
        );
    }
}

error_reporting(E_ALL);

testDataDecodeAndItsOk();
testDataEncodeAndItsOk();
