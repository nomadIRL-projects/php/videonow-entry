# videonow-entry

## Описание
Тестовое задание для компании Videonow (https://gitlab.videonow.ru/-/snippets/1)

<details>
<summary>Оригинальный текст задания</summary>

```php
<?php

$data1 = [
    'parent.child.field' => 1,
    'parent.child.field2' => 2,
    'parent2.child.name' => 'test',
    'parent2.child2.name' => 'test',
    'parent2.child2.position' => 10,
    'parent3.child3.position' => 10,
];

$data2 = [
    'parent' => [
        'child' => [
            'field' => 1,
            'field2' => 2,
        ],
    ],
    'parent2' => [
        'child' => [
            'name' => 'test',
        ],
        'child2' => [
            'name' => 'test',
            'position' => 10,
        ],
    ],
    'parent3' => [
        'child3' => [
            'position' => 10,
        ],
    ],
];

/*
 * Реализовать интерфейс для пребразования data1 <=> data2
 */
interface IArrayUtil {

    /*
     * Преобразования ключ/значение в многомерный ассоциативный массив
     */
    public static function data_decode(array $data): array;

    /*
     * Кодирование ассоциативного многомерного массива в key/value
     */
    public static function data_encode(array $data): array;
}

```

</details>

## Системные требования
* PHP 7.4 (или онлайн интерпретатор https://onlinephp.io/)

## Запуск
Можно запустить юнит-тесты:
```shell
php tests/common/unit/ArrayConverterTest.php
```
Если ошибок нет, консоль будет пуста. Если появятся ошибки, в консоли будет выведено предупреждение.

Для добавления собственных тест-кейсов необходимо соблюдать одинаковое количество тест-кейсов в обоих дата-провайдерах (для `encodeData` и `decodeData`)