<?php

namespace domain;

/*
 * Реализовать интерфейс для преобразования data1 <=> data2
 */
interface IArrayUtil {

    /*
     * Преобразования ключ/значение в многомерный ассоциативный массив
     */
    public static function dataDecode(array $data): array;

    /*
     * Кодирование ассоциативного многомерного массива в key/value
     */
    public static function dataEncode(array $data): array;
}
